import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'h-app',
  template: `
            <h1>Component Interaction in {{parentData}} </h1>
            <button (click)="eventt()" >Child to Parent</button>
  `
})
export class InteractComponent {
    @Input() parentData = '';
    @Output() childEvent = new EventEmitter();
    
    eventt(){
      this.childEvent.emit("Angular v11");
    }
}
