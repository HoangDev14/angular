import { Component } from '@angular/core';

@Component({
  selector: 'hoang',
  template: `<h3>Interpolation: {{inter}}</h3>
             <br/>
             <h3>Event Binding: </h3>
            <button (click) = "eBinding()">Click me</button>
  `
})
export class DemoComponent {
    inter = 'Hello Angular';
    
    eBinding(){
        this.inter = 'Day la event binding';
    }
}
